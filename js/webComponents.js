// web component para botones

class MiBoton extends HTMLElement{
    constructor() {
        super();
            var form = document.getElementById("form-id");
            //document.getElementById("search-button").addEventListener('click', function(e){
            //this.addEventListener('click', function(e){    
                //form.submit();
            //})
            // alert('buscando');
        //});
    }
}

customElements.define('mi-boton', MiBoton);

// segundo botón

customElements.define('mi-boton-2', class extends HTMLElement{
    constructor() {
        super();
        var form = document.getElementById("form-id");

        //document.getElementById("clear-button").addEventListener('click', function(e){
        this.addEventListener('click', function(e){   
            form.reset();
        });
    }
});


// Fetch request

class MiTabla extends HTMLElement{
    constructor() {
        super();
        fetch('http://jsonplaceholder.typicode.com/users')
        .then(res => res.json())
        .then(data => showData(data))
        .catch(e => console.log(e))

        const showData = (data) => {
            console.log(data)
            // let body = ''
            // for(let i = 0; i < data.length; i++){
            //     body += `<tr><td>${data[i].id}</td><td>${data[i].name}</td><td>${data[i].username}</td></tr>`
            // }

            // document.getElementById('data').innerHTML = body;
            const users = data;
            const search = document.querySelector('#mySearch');
            const boton = document.querySelector('#search-button');
            const result = document.querySelector('#data')

            const filtrar = () => {
                result.innerHTML = ''
                //console.log(search.value)
                const text = search.value.toLowerCase();
                for(let user of users){
                    let name = user.name.toLowerCase();

                    // comparo si en data(users) existe algo parecido con lo que introduce el usuario
                    if(name.includes(text)){ 
                        result.innerHTML +=`<tr><td>${user.id}</td><td>${user.name}</td><td>${user.username}</td></tr>` // lo muestro agregandolo a la tabla
                    }

                }
                if (result.innerHTML === ''){

                    result.innerHTML += `<tr><td> User not found ... </td> ` // sino se encuentran coincidencias
                }
            }

            boton.addEventListener('click', filtrar); // al hacer click
            search.addEventListener('keyup', filtrar); // mientras voy escribiendo
            filtrar();  // para mostrar la tabla con los datos completos desde el inicio
        }

    }
}

customElements.define('mi-tabla', MiTabla);


